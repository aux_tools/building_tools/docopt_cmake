cmake_minimum_required(VERSION 3.10)

set(MODEL TRUE CACHE BOOL "")

project(docopt_cli C)

#
# Для сборки требуется указать в переменной DOCOPT_CONF путь к файлу docopt с
# описанием интерфейса
#
execute_process(
    COMMAND docopt_c/docopt_c.py -t ${CMAKE_CURRENT_SOURCE_DIR}/templates/template.c
                                 -p ${CMAKE_CURRENT_SOURCE_DIR}/templates/template.h
                                 -o ${PROJECT_NAME}.c
                                    ${DOCOPT_CONF}
    WORKING_DIRECTORY   ${CMAKE_CURRENT_SOURCE_DIR}
    )

#
# Генерируемые файлы 
#
file (GLOB DOCOPT_PARSER ${PROJECT_NAME}.c ${PROJECT_NAME}.h)

# 
# Строим библиотеку
#
add_library(${PROJECT_NAME}
    ${DOCOPT_PARSER}
    )
#
# Отключим некоторые варнинги создаваемые при сборке шаблонного кода
#
target_compile_options(${PROJECT_NAME}
    PRIVATE "-Wno-unused-parameter"
    PRIVATE "-Wno-cast-qual"
)
