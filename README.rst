Docopt-c CMake wrapper
###############################################################################
Проект обертки над оригинальным `docopt.c`_.

Проект добавляет обертку на CMake позволяющую собирать генерируемый код как
библиотеку в основной проект. Кроме этого добавлены кастомные файлы шаблонов,
представляющие собой оригинальные файлы но без вызова "exit()" в случае ошибки в
вводимых аргументах.


.. ============================================================================

.. _`docopt.c`:
   https://github.com/docopt/docopt.c
